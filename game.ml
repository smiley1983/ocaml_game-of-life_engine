open Td;;

let const_height = 29;;

let const_width = 29;;

let char_of_tile = function
 | `Black -> 'b'
 | `White -> 'w'
 | `Empty -> '-'
;;

let render game bot =
  let result = ref (Printf.sprintf "%c\n" bot.symbol) in
  Array.iter (fun row -> Array.iter (
      fun t -> result := !result ^ (Debug.string_of_tile t)
  ) row; result := !result ^ "\n") game.grid;
    (!result ^ "\n")
;;

let read_orders chan =
  Io.input_order chan
;;

let finished game =
  game.turn >= 80
;;

let update game orders =
  game.turn <- game.turn + 1;
  List.iter (fun (color, (row, col)) ->
    if game.grid.(row).(col) = `Empty then
      game.grid.(row).(col) <- color
    else
      failwith (Printf.sprintf "bad order from %c to %d, %d" 
          (char_of_tile color) row col)
  ) orders
;;

let log game orders =
  Debug.log (Printf.sprintf "Turn = %d\n" game.turn);
  List.iter (fun (color, (row, col)) ->
    Debug.log (Printf.sprintf "%c %d %d\n" (char_of_tile color) row col)
  ) orders;
  Array.iter (fun row -> Array.iter (
      fun t -> Debug.log (Debug.string_of_tile t)
  ) row; Debug.log "\n") game.grid;
  Debug.flush_log ();
  let filename = 
    Printf.sprintf "replay_%03d.png" (game.turn)
  in
    Render.write_grid filename game.grid

;;

let new_player cmd id =
  let mtile = match id with 'b' -> `Black | _ -> `White in
   {
    symbol = id;
    color = mtile;
    cmd = cmd;
   }
;;

let new_state () =
 {
  grid = Array.make_matrix const_height const_width `Empty;
  height = const_height;
  width = const_width;
  turn = 0
 }
;;

