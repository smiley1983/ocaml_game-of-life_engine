open Td;;

let debug s = output_string stderr s; flush stderr;;

let debug_char c = output_char stderr c; flush stderr;;

let log s = output_string stdout s;;

let string_of_tile = function
 | `Black -> "b"
 | `White -> "w"
 | `Empty -> "-"
;;

let log_grid grid =
  Array.iter (fun row -> Array.iter (
      fun t -> log (string_of_tile t)
  ) row; log "\n") grid;
;;

let flush_log () = flush stdout;;
