open Td;;

let all_dirs : dir array = [| `N ; `NE; `E ; `SE; `S ; `SW; `W; `NW |];;

let dir_offset = function
  | `N -> (-1, 0)
  | `NE -> (-1, 1)
  | `E -> (0, 1)
  | `SE -> (1, 1)
  | `S -> (1, 0)
  | `SW -> (1, -1)
  | `W -> (0, -1)
  | `NW -> (-1, -1)
;;

type sim =
 {
  state : state;
  mutable target : tile array array;
  sim_turns : int;
  mutable turn_count : int;
 }
;;

let is_finished sim =
  (sim.turn_count = sim.sim_turns)
;;

let in_bounds state (row, col) =
  row > 0
  && col > 0
  && row < state.height
  && col < state.width
;;

let get_neighbors state (row, col) =
  Array.map (fun dir ->
    let orow, ocol = dir_offset dir in
    let nrow, ncol = (row + orow), (col + ocol) in
    if in_bounds state (nrow, ncol) then
      Some state.grid.(nrow).(ncol)
    else None
  ) all_dirs
;;

let count_neighbors grid n =
  Array.fold_left (fun (black, white) o ->
    match o with
    | None -> black, white
    | Some p ->
      match p with
      | `Empty -> black, white
      | `Black -> black + 1, white
      | `White -> black, white + 1
  ) (0, 0) n
;;

let update_cell state target (row, col) cell changed =
  let neighbors = get_neighbors state (row, col) in
  let black, white = count_neighbors state.grid neighbors in
  let total = black + white in
  match total with
  | 3 -> 
      if cell = `Empty then
       (
        changed := true;
        let switch_to = if black > white then `Black else `White in
          target.(row).(col) <- switch_to
       )
      else ()
  | 2 -> ()
  | _ ->
    if not (state.grid.(row).(col) = `Empty) then
     (
      changed := true;
      target.(row).(col) <- `Empty
     )
;;

let update_sim sim =
  sim.turn_count <- sim.turn_count + 1;
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    sim.target.(ir).(ic) <- `Empty
  ) row) sim.target;
  let changed = ref false in
  Array.iteri (fun ir row -> Array.iteri (fun ic cell ->
    update_cell sim.state sim.target (ir, ic) cell changed
  ) row) sim.state.grid;
  if not !changed then sim.turn_count <- sim.sim_turns
  else
   ( 
    let swap_grid = sim.state.grid in
      sim.state.grid <- sim.target;
      sim.target <- swap_grid
   )
;;

let run_sim sim ascii_log png_log =
  while sim.turn_count < sim.sim_turns do
    if ascii_log then
     (
      Debug.log (Printf.sprintf "\nSim turn %d\n" sim.turn_count);
      Debug.log_grid sim.state.grid
     );
    if png_log then
     (
      let filename = 
        Printf.sprintf "replay_%03d.png" (sim.turn_count + 80)
      in
        Render.write_grid filename sim.state.grid
     );
    update_sim sim
  done
;;

let new_sim state target sim_turns =
 {
  state = state;
  target = target;
  sim_turns = sim_turns;
  turn_count = 0;
 }
;;

let simulate permastate source target turns =
  let state = 
   {permastate with grid = source; 
    height = (Array.length source); 
    width = (Array.length source.(0))
   } 
  in
    run_sim (new_sim state target turns) true true
;;

