type tile = [ `Black | `White | `Empty ];;

type dir = [ `N | `E | `S | `W | `NE | `SE | `NW | `SW ];;

type player =
 {
  symbol : char;
  color : tile;
  cmd : string;
 }
;;

type state =
 {
  mutable grid : tile array array;
  height : int;
  width : int;
  mutable turn : int;
 }
;;

type game =
 {
  players : player list;
  player1 : player;
  player2 : player;
  model : state;
 }
;;


