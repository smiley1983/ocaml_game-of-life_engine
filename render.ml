open Printf;;

let write_png filename data =
  let width = Array.length data.(0) in
  let height = Array.length data in 
  let chan = Unix.open_process_out ("pnmtopng > " ^ filename) in
  fprintf chan "P3 %d %d 255\n" width height;
  Array.iter 
    (Array.iter (fun (r, g, b) -> 
      fprintf chan "%d %d %d\n" r g b
    )) data;
  ignore (Unix.close_process_out chan)
;;

let image_of_tiles grid =
  Array.map (Array.map (function
    | `Black -> (32, 32, 32)
    | `White -> (224, 224, 224)
    | `Empty -> (128, 128, 128)
  )) grid
;;

let scale_matrix row_scale col_scale m =
  let old_rows = Array.length m in
  let old_cols = Array.length m.(0) in
  let rows, cols = old_rows * row_scale, old_cols * col_scale in
  let new_matrix = Array.make_matrix rows cols m.(0).(0) in
    Array.iteri (fun ir row -> 
      Array.iteri (fun ic cell ->
        for count_row = 0 to (row_scale - 1) do
          for count_col = 0 to (col_scale - 1) do
            let target_row = (ir * row_scale) + count_row in
            let target_col = (ic * col_scale) + count_col in
              new_matrix.(target_row).(target_col) <- cell
          done
        done
      ) row
    ) m;
    new_matrix
;;

let write_grid filename grid =
  let image = image_of_tiles grid in
    write_png filename (scale_matrix 10 10 image)
;;

(*write_png "test.png" [| [|(255,255,255)|] |];; *)
