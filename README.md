Test two Game of Life bots against each other (see hackerrank Game of life
challenge for details)

Build with

ocamlbuild -lib unix playgame.native

Run with

./playgame.native ./bot-cmd ./bot-cmd
